package pl.szatek.service;

import org.springframework.http.ResponseEntity;
import pl.szatek.model.ReturnToken;
import pl.szatek.model.Token;

public interface TokenService {
    ResponseEntity<ReturnToken> encryptToken(Token token);
    ResponseEntity<ReturnToken> decryptToken(Token token);
}
