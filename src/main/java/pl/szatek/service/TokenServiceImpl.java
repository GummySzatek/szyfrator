package pl.szatek.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.szatek.exceptions.NotAcceptableException;
import pl.szatek.model.ReturnToken;
import pl.szatek.model.Token;
import pl.szatek.util.EncryptionUtil;

@Service
public class TokenServiceImpl implements TokenService {

    @Override
    public ResponseEntity<ReturnToken> encryptToken(Token token) {
        ReturnToken returnToken = null;
        try {
            EncryptionUtil encryptionUtil = new EncryptionUtil();
            String newToken = encryptionUtil.encrypt(token.getToken(), token.getKey());
            returnToken = new ReturnToken(newToken);
        } catch (Exception e) {
            e.printStackTrace();
            throw new NotAcceptableException();
        }
        return new ResponseEntity(returnToken, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ReturnToken> decryptToken(Token token) {
        ReturnToken returnToken = null;
        try {
            EncryptionUtil encryptionUtil = new EncryptionUtil();
            String newToken = encryptionUtil.decrypt(token.getToken(), token.getKey());
            returnToken = new ReturnToken(newToken);
        } catch (Exception e) {
            e.printStackTrace();
            throw new NotAcceptableException();
        }
        return new ResponseEntity(returnToken, HttpStatus.OK);
    }
}
