package pl.szatek.model;

public class ReturnToken {
    private String token;

    public ReturnToken() {
    }

    public ReturnToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
