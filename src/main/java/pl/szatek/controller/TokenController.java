package pl.szatek.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.szatek.model.ReturnToken;
import pl.szatek.model.Token;
import pl.szatek.service.TokenService;

@RestController
public class TokenController {

    @Autowired
    private TokenService tokenService;

    @RequestMapping(value = "/token/encrypt", method = RequestMethod.POST)
    public ResponseEntity<ReturnToken> encryptToken(@RequestBody Token token) {
        return tokenService.encryptToken(token);
    }

    @RequestMapping(value = "/token/decrypt", method = RequestMethod.POST)
    public ResponseEntity<ReturnToken> decrypttToken(@RequestBody Token token) {
        return tokenService.decryptToken(token);
    }
}
