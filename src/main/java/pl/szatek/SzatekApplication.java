package pl.szatek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SzatekApplication {

    public static void main(String[] args) {
        SpringApplication.run(SzatekApplication.class, args);
    }
}
